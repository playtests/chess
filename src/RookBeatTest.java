package chess.chess;


import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class RookBeatTest {

	private Board board;
	private Rook Rook;
	private Rook anotherRook;
	
	private Position from;
	private Position to;
	private boolean expectedResult;
	private String color;
	private String anotherColor;
	
	public RookBeatTest(Position from, Position to, String color, String anotherColor, boolean expectedResult) {
		this.from = from;
		this.to = to;
		this.color = color;
		this.anotherColor = anotherColor;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp() {
		//inicjacja szachownicy
		board = new Board();
		
		//inicjacja białego pionka żywego nieruszanego na pozycji d2
		Position position = new Position("d4");
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		Rook = new Rook(color, position, isAlive, hasMove, board);
		
		anotherRook = new Rook(color, position, isAlive, hasMove, board);
		
	}
	
	@Parameterized.Parameters
	public static Collection params() {
		return Arrays.asList(new Object[][] {
				//bicie prawidłowe
				{new Position("a1"), new Position("a2"),"white", "black", true},	
				{new Position("c2"), new Position("c1"),"black", "white", true},	
				{new Position("a1"), new Position("c1"), "black","white", true},	
				{new Position("g6"), new Position("g3"),"white", "black", true},	
				//ruch o więcej niż jedno pole nieprawidłowy
				{new Position("c1"), new Position("c4"), "white", "white", false},	
				{new Position("h8"), new Position("h1"), "black", "black", false},	
				{new Position("c4"), new Position("f4"), "black", "black", false},	
				{new Position("f1"), new Position("a1"), "white", "white", false},	
					
		});
	}
	
	
	@Test
	public void testRookMove() {
		//ustawiamy kolor i pozycję gońca
		Rook.setPosition(from);
		Rook.setColor(color);
		//ustawiamy pozycję i kolor drugiej figury
		anotherRook.setPosition(to);
		anotherRook.setColor(anotherColor);
		
		boolean result = Rook.move(to);
		
		assertEquals(expectedResult, result);
	}

}
