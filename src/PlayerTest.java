package chess.chess;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

//import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class PlayerTest {

	Player player;
	
	
	@Mock
	Timer timer;

	Board board;
	
	@Before
	public void setUp() {
		String color = "white";
		timer = mock(Timer.class);
		board = new Board();
		player = new Player(color, board, timer);
		
		Mockito.when(timer.getTime("white")).thenReturn(2000);
	}
	
	
	


	@Test
	public void testMoveHasFigureCorrectTurnCorrectTimeSuccess() {
		board.setTurn("white");
		
		Position from = new Position("b2");
		Position to = new Position("b3");
		//inicjalizacja figury
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		
		Pawn pawn = new Pawn(color, from, isAlive, hasMove, board);
		
		boolean result = player.doMove(from, to);
		
		assertTrue(result);
	}
	
	@Test
	public void testMoveHasFigureInCorrectTurnCorrectTimeFailure() {
		board.setTurn("black");
		
		Position from = new Position("b2");
		Position to = new Position("b3");
		
		//inicjalizacja figury
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		
		Mockito.when(timer.getWhiteTime()).thenReturn(2000);
		
		Pawn pawn = new Pawn(color, from, isAlive, hasMove, board);
		
		boolean result = player.doMove(from, to);
		
		assertFalse(result);
	}
	
	@Test
	public void testMoveHasNoFigureCorrectTurnCorrectTimeFailure() {
		board.setTurn("black");
		
		Position from = new Position("b2");
		Position to = new Position("b3");
		Position pawnPosition = new Position("d4");
		//inicjalizacja figury
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		
		Mockito.when(timer.getWhiteTime()).thenReturn(2000);
		
		Pawn pawn = new Pawn(color, pawnPosition, isAlive, hasMove, board);
		
		boolean result = player.doMove(from, to);
		
		assertFalse(result);
	}
	
	@Test
	public void testMoveHasWrongFigureCorrectTurnCorrectTimeFailure() {
		board.setTurn("black");
		
		Position from = new Position("b2");
		Position to = new Position("b3");
		//inicjalizacja figury
		String color = "black";
		boolean isAlive = true;
		boolean hasMove = true;
		
		Mockito.when(timer.getWhiteTime()).thenReturn(2000);
		
		Pawn pawn = new Pawn(color, from, isAlive, hasMove, board);
		
		boolean result = player.doMove(from, to);
		
		assertFalse(result);
	}

	@Test
	public void testMoveHasFigureCorrectTurnCorrectTimeWrongMoveFailure() {
		board.setTurn("black");
		
		Position from = new Position("b2");
		Position to = new Position("f7");
		//inicjalizacja figury
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		
		Mockito.when(timer.getWhiteTime()).thenReturn(2000);
		
		Pawn pawn = new Pawn(color, from, isAlive, hasMove, board);
		
		boolean result = player.doMove(from, to);
		
		assertFalse(result);
	}
}
