//import java.util.Arrays;
//import java.util.Collection;
// 
//
//import org.junit.Test;
//import org.junit.Before;
//import org.junit.runners.Parameterized;
//import org.junit.runners.Parameterized.Parameters;
//import org.junit.runner.RunWith;
//
//import static org.junit.Assert.*;
//
//@RunWith(Parameterized.class)
//public class paramTest {
//   private Integer inputNumber;
//   private Boolean expectedResult;
//   private Pawn primeNumberChecker;
//
//   @Before
//   public void initialize() {
////      primeNumberChecker = new Pawn();
//   }
//
//   // Each parameter should be placed as an argument here
//   // Every time runner triggers, it will pass the arguments
//   // from parameters we defined in primeNumbers() method
//   public paramTest(Integer inputNumber, 
//      Boolean expectedResult) {
//      this.inputNumber = inputNumber;
//      this.expectedResult = expectedResult;
//   }
//
//   @Parameterized.Parameters
//   public static Collection primeNumbers() {
//      return Arrays.asList(new Object[][] {
//         { 2, true },
//         { 6, false },
//         { 19, true },
//         { 22, false },
//         { 23, true }
//      });
//   }
//
//   // This test will run 4 times since we have 5 parameters defined
//   @Test
//   public void testPrimeNumberChecker() {
//      System.out.println("Parameterized Number is : " + inputNumber);
//      assertEquals(expectedResult, 
//      //primeNumberChecker.validate(inputNumber));
//    	inputNumber == 2);	  
//   }
//   
//   @Test
//   public void anotherTest() {
//	   assertTrue(true);
//   }
//}
