package chess.chess;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class RookMoveWithAnotherPieceTest {

	private Board board;
	private Rook rook;
	private Rook anotherRook;
	
	private Position from;
	private Position to;
	private Position anotherRookPosition;
	private boolean expectedResult;
	
	public RookMoveWithAnotherPieceTest(Position from, Position to, Position anotherRookPosition, boolean expectedResult) {
		this.from = from;
		this.to = to;
		this.anotherRookPosition = anotherRookPosition;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp() {
		//inicjacja szachownicy
		board = new Board();
		
		//inicjacja białego pionka żywego nieruszanego na pozycji d2
		Position position = new Position("d4");
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		rook = new Rook(color, position, isAlive, hasMove, board);
		
		anotherRook = new Rook(color, position, isAlive, hasMove, board);
		
	}
	
	@Parameterized.Parameters
	public static Collection params() {
		return Arrays.asList(new Object[][] {
				//ruch o jedno pole
				{new Position("a1"), new Position("b1"),new Position("b3"), true},	
				{new Position("c2"), new Position("b2"),new Position("h8"), true},	
				//ruch o więcej niż jedno pole prawidłowy
				{new Position("a1"), new Position("a3"), new Position("g2"), true},	
				{new Position("g6"), new Position("g3"), new Position("a1"), true},	
				//ruch o więcej niż jedno pole nieprawidłowy
				{new Position("c1"), new Position("f1"), new Position("d1"), false},	
				{new Position("h8"), new Position("a8"), new Position("c8"), false},	
				{new Position("c4"), new Position("c1"), new Position("c2"), false},	
				{new Position("f1"), new Position("f6"), new Position("f2"), false},	
					
		});
	}
	
	
	@Test
	public void testRookMove() {
		//ustawiamy pozycję gońca
		rook.setPosition(from);
		//ustawiamy pozycję drugiej figury
		anotherRook.setPosition(anotherRookPosition);
		
		boolean result = rook.move(to);
		
		assertEquals(expectedResult, result);
	}

}
