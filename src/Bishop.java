package chess.chess;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;


public class Bishop extends Piece{

	public Bishop(String color, Position position, boolean isAlive,
			boolean hasMove, Board board) {
		super(color, position, isAlive, hasMove, board);
		// TODO Auto-generated constructor stub
	}
	
	boolean isBoardCellEmpty(Position position) {
		return board.getCell(position).getPiece() == null;
	}

	boolean areCellsEmpty(Position destination) {
		int orderX;
		int orderY;
		
		if(this.position.getX() <= destination.getX()) {
			orderX = 1;
		} else {
			orderX = -1;
		}
		
		if(this.position.getY() <= destination.getY()) {
			orderY = 1;
		} else {
			orderY = -1;
		}
		
		for(int i= 1; i<Math.abs(this.position.getX()- destination.getX()); i++) {
				int currentX = this.position.getX() + orderX * i;
				int currentY = this.position.getY() + orderY * i;
				if(!isBoardCellEmpty(new Position(currentX, currentY))) {
					return false;
				}
		}
		return true;
	}
	
	@Override
	boolean move(Position destination) {
		//bicie
		if(Math.abs(this.position.getX() - destination.getX()) == 
		   Math.abs(this.position.getY() - destination.getY()) &&
		   areCellsEmpty(destination) && 
		   (!isBoardCellEmpty(destination) && this.board.getCell(destination).getPiece().getColor() != this.color) ) {
			
			setBeatenPieceParams(destination);
			setParamsAfterMove(destination);
			
			return true;
			//dodać ustawianie parametrów
			
		} else {
			//niebicie
			if(Math.abs(this.position.getX() - destination.getX()) == 
					   Math.abs(this.position.getY() - destination.getY()) &&
					   areCellsEmpty(destination) &&
					   isBoardCellEmpty(destination)) {
				
				setParamsAfterMove(destination);
				
				return true;
			} else {
				
				//błąd
				return false;
			}
		}
		
	}
	
	
}
