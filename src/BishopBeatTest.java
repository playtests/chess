package chess.chess;


import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class BishopBeatTest {

	private Board board;
	private Bishop bishop;
	private Bishop anotherBishop;
	
	private Position from;
	private Position to;
	private boolean expectedResult;
	private String color;
	private String anotherColor;
	
	public BishopBeatTest(Position from, Position to, String color, String anotherColor, boolean expectedResult) {
		this.from = from;
		this.to = to;
		this.color = color;
		this.anotherColor = anotherColor;
		this.expectedResult = expectedResult;
	}
	
	@Before
	public void setUp() {
		//inicjacja szachownicy
		board = new Board();
		
		//inicjacja białego pionka żywego nieruszanego na pozycji d2
		Position position = new Position("d4");
		String color = "white";
		boolean isAlive = true;
		boolean hasMove = true;
		bishop = new Bishop(color, position, isAlive, hasMove, board);
		
		anotherBishop = new Bishop(color, position, isAlive, hasMove, board);
		
	}
	
	@Parameterized.Parameters
	public static Collection params() {
		return Arrays.asList(new Object[][] {
				//bicie prawidłowe
				{new Position("a1"), new Position("b2"),"white", "black", true},	
				{new Position("c2"), new Position("b1"),"black", "white", true},	
				{new Position("a1"), new Position("c3"), "black","white", true},	
				{new Position("g6"), new Position("d3"),"white", "black", true},	
				//ruch o więcej niż jedno pole nieprawidłowy
				{new Position("c1"), new Position("f4"), "white", "white", false},	
				{new Position("h8"), new Position("a1"), "black", "black", false},	
				{new Position("c4"), new Position("f1"), "black", "black", false},	
				{new Position("f1"), new Position("a6"), "white", "white", false},	
					
		});
	}
	
	
	@Test
	public void testBishopMove() {
		//ustawiamy kolor i pozycję gońca
		bishop.setPosition(from);
		bishop.setColor(color);
		//ustawiamy pozycję i kolor drugiej figury
		anotherBishop.setPosition(to);
		anotherBishop.setColor(anotherColor);
		
		boolean result = bishop.move(to);
		
		assertEquals(expectedResult, result);
	}

}
